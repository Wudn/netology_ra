'use strict';

Array.prototype.groupBy = function(prop) {
    return this.reduce(function(groups, item) {
        const val = item[prop];
        groups[val] = groups[val] || [];
        groups[val].push(item);
        return groups;
    }, {});
}

// Сгруппировать по месяцам
function groupByMonths(list) {
    // Отформатировать дату
    const formattedDate = list.map(item => {
        const monthIndex = new Date(item.date).getMonth() + 1;
        const monthShortName = new Date(item.date).toLocaleString('en-us', { month: 'short' });
        return {
            ...item,
            'monthShortName': monthShortName,
            'monthIndex': monthIndex
        }
    });
    
    let groupped = [];
    let sorted = [];
    if (formattedDate.length > 0) {
        groupped = formattedDate.groupBy('monthShortName');
        
        for (let key in groupped) {
            const monthIndex = Math.max(...groupped[key].map(item => item.monthIndex));
            const amount = groupped[key]
                .map(item => item.amount)
                .reduce((total, val) => total + val);
                sorted.push({month: key, amount: amount, monthIndex: monthIndex});
        }
        
        // Отсортировать месяцы
        sorted.sort((a, b) => {
            if (a.monthIndex > b.monthIndex) {
                return 1;
            }
            if (a.monthIndex < b.monthIndex) {
                return -1;
            }
            // a должно быть равным b
            return 0;
        });

        sorted = sorted.map(item => {return {month: item.month, amount: item.amount}});
    }
    
    return sorted;
}

// Сгруппировать по годам
function groupByYears(list) {
    // Отформатировать дату
    const formattedDate = list.map(item => {
        const year = new Date(item.date).getFullYear();
        return {
            year: year,
            amount: item.amount
        }
    });

    const groupped = formattedDate.groupBy('year');
    let sorted = [];

    for (let key in groupped) {
        const amount = groupped[key]
        .map(item => item.amount)
        .reduce((total, val) => total + val);
        sorted.push({year: key, amount: amount});
    }

    // Отсортировать месяцы
    sorted.sort((a, b) => {
        if (a.year > b.year) {
            return 1;
        }
        if (a.year < b.year) {
            return -1;
        }
        // a должно быть равным b
        return 0;
    });

    return sorted;
}

// Сортировать по убыванию
function sortByDesc(list) {
    // Отсортировать месяцы
    list.sort((a, b) => {
        return b.amount - a.amount;
    });
    return list;
}

const withPreTreatment = action => Component => class extends React.Component {
    // Конструктор
    constructor(props) {
        super(props);
    }

    render() {
        const result = action(this.props.list);
        return <Component  {...this.props} list={result} />;
    }
};

const WrappedMonthTable = withPreTreatment(groupByMonths)(MonthTable);
const WrappedYearTable = withPreTreatment(groupByYears)(YearTable);
const WrappedSortTable = withPreTreatment(sortByDesc)(SortTable);

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        };
    }

    componentDidMount() {
        axios.get('https://api.myjson.com/bins/l2s9l').then(response => {
            this.setState(response.data);
        });
    }

    render() {
        return (
            <div id="app">
                <WrappedMonthTable list={this.state.list} />
                <WrappedYearTable list={this.state.list} />
                <WrappedSortTable list={this.state.list} />
            </div>
        );
    }
};