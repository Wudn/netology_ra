'use strict';

const withCheckViews = () => Component => class extends React.Component {

    render() {
        const node = <Component {...this.props} />;
        if (this.props.views < 100) {
            return (
                <New>
                    {node}
                </New>
            )
        }
        if (this.props.views > 1000) {
            return (
                <Popular>
                    {node}
                </Popular>
            )
        }
        return node
    }
};

const WrappedVideo = withCheckViews()(Video);
const WrappedArticle = withCheckViews()(Article);

const List = props => {
    return props.list.map(item => {
        switch (item.type) {
            case 'video':
                return (
                    <WrappedVideo {...item} />
                    // <Video {...item} />
                );

            case 'article':
                return (
                    <WrappedArticle {...item} />
                    // <Article {...item} />
                );
        }
    });
};
