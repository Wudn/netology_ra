'use strict';

const withFormatDate = Component => class extends React.Component {
    // Конструктор
    constructor(props) {
        super(props);
    }

    // Склонение числительных в javascript
    declOfNum(number, titles) {  
        const cases = [2, 0, 1, 1, 1, 2];  
        return titles[ (number%100>4 && number%100<20) ? 2 : cases[(number%10<5)?number%10:5] ];  
    }

    // Получить разницу в секундах
    getSecondsDiff(date) {
        const dateTimestamp = new Date(date).getTime();
        const curDateTimestamp = Date.now();
        return (curDateTimestamp - dateTimestamp)/1000;
    }

    // Получить разницу в минутах
    getMinutesDiff(seconds) {
        return +Number(seconds/60).toFixed(0);
    }

    // Получить разницу в часах
    getHoursDiff(minutes) {
        return +Number(minutes/60).toFixed(0);
    }

    // Получить разницу в днях
    getDaysDiff(hours) {
        return +Number(hours/24).toFixed(0);
    }

    // Проверить условия
    checkTime(minutes) {
        const minutesDict = ['минута', 'минуты', 'минут'];
        const hoursDict = ['час', 'часа', 'часов'];
        const daysDict = ['день', 'дня', 'дней'];

        if (minutes > 0 && minutes < 60) {
            return `${minutes} ${this.declOfNum(minutes, minutesDict)} назад`;
        }
        if (minutes >= 60 && minutes <= 60*24) {
            const hours = this.getHoursDiff(minutes);
            return `${hours} ${this.declOfNum(hours, hoursDict)} назад`;
        }
        const days = this.getDaysDiff(this.getHoursDiff(minutes));
        return `${days} ${this.declOfNum(days, daysDict)} назад`;
    }
    
    // Форматировать дату
    formatDate() {
        const date = this.props.date;
        const seconds = this.getSecondsDiff(date);
        const minutes = this.getMinutesDiff(seconds);
        return this.checkTime(minutes);
    }
    
    render() {
        const formattedDate = this.formatDate();
        return <Component  {...this.props} date={formattedDate} />;
    }
};

const DateTimePretty = withFormatDate(DateTime);

const Video = props => {
    return (
        <div className="video">
            <iframe src={props.url} frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            <DateTimePretty date={props.date} />
        </div>
    )
};