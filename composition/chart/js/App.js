class App extends React.Component {

	// static defaultProps = {
  //   data: []
  // }
	constructor(props) {
		super(props)
		// console.log('AppProps:', this.props);
		// console.log('state', this.state);
		this.state = {
			data: [],
			series: ['France', 'Italy', 'England', 'Sweden', 'Germany'],
			labels: ['cats', 'dogs', 'horses', 'ducks', 'cows'],
			colors: ['#43A19E', '#7B43A1', '#F2317A', '#FF9824', '#58CF6C']
		}
	}

	// componentWillMount() {
	// 	this.setState({
	// 		data: [],
	// 		series: ['France', 'Italy', 'England', 'Sweden', 'Germany'],
	// 		labels: ['cats', 'dogs', 'horses', 'ducks', 'cows'],
	// 		colors: ['#43A19E', '#7B43A1', '#F2317A', '#FF9824', '#58CF6C']
	// 	})
	// }

	componentDidMount() {
		this.populateArray();
		setInterval(this.populateArray.bind(this), 2000);
	}

	getMax() {
		return this.state.data.reduce((max, serie) => Math.max(max, serie.reduce((serieMax, item) => Math.max(serieMax, item), 0)), 0);
	}

	getSum(serie) {
		return serie.reduce((carry, current) => carry + current, 0);
	}

	getIndexOfSortedSerie(serie, item) {
		let sortedSerie = serie.slice(0);
		sortedSerie.sort((a, b) => a - b);
		return sortedSerie.indexOf(item);
	}

	getStyle(item, idx, styleOptions) {
		// console.log('testStyle', styleOptions);
		let color = this.state.colors[idx], 
			style;
		
		// Изменяемый объект
		// В просы принимаю опции -> создаю новый объект -> извлекаю свойства из объекта по умолчанию -> добавляю новые свойства
		style = {
			backgroundColor: color,
			zIndex: item
		};

		style = {
			...style,
			...styleOptions
		}

		// console.log('style', style);

		return style;
	}

	getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	}

	populateArray() {
		const	series = 5;
		const serieLength = 5;

		let data = new Array(series).fill(new Array(serieLength).fill(0));
		data = data.map(serie => serie.map(item => this.getRandomInt(0, 20)));

		this.setState({ data });
	}
	
	render() {
		return (
			<section>
				<Charts data={this.state.data}>
					{data => data.map((serie, serieIndex) => {
						return (
							<ChartsSerie key={ serieIndex } serie={serie} label={this.state.labels[serieIndex]}>
								{serie => serie.map((item, itemIndex) => {
									const size = item/(this.getMax()) * 100;
									const style = {
										height: size + '%',
										opacity: item/this.getMax() + .05
									}
									return (
										<ChartsItem style={this.getStyle(item, itemIndex, style)} key={itemIndex} item={item} color={this.state.colors[itemIndex]}/>
									)
								})}
							</ChartsSerie>
						)
						})
					}
				</Charts>
				
				<Charts data={this.state.data}>
					{data => data.map((serie, serieIndex) => {
						return (
							<ChartsSerie additionalClasses={['stacked']} key={ serieIndex } serie={serie} label={this.state.labels[serieIndex]}>
								{serie => serie.map((item, itemIndex) => {
									const style = {
										height: item/(this.getSum(serie)) * 100 + '%',
										opacity: 1
									}
									return <ChartsItem additionalClasses={'stacked'} style={this.getStyle(item, itemIndex, style)} key={itemIndex} item={item} color={this.state.colors[itemIndex]}/>
								})}
							</ChartsSerie>
						)
						})
					}
				</Charts>

				<Charts data={this.state.data}>
					{data => data.map((serie, serieIndex) => {
						return (
							<ChartsSerie additionalClasses={['layered']} key={ serieIndex } serie={serie} label={this.state.labels[serieIndex]}>
								{serie => serie.map((item, itemIndex) => {
									const style = {
										height: item/(this.getMax()) * 100 + '%',
										opacity: (item/(this.getMax()) + .05),
										right: ((this.getIndexOfSortedSerie(serie, item)/ (serie.length + 1)) * 100) + '%'
									}
									return <ChartsItem additionalClasses={'layered'} style={this.getStyle(item, itemIndex, style)} key={itemIndex} item={item} color={this.state.colors[itemIndex]}/>
								})}
							</ChartsSerie>
						)
						})
					}
				</Charts>

				<Charts additionalClasses={['horizontal']} data={this.state.data}>
					{data => data.map((serie, serieIndex) => {
						return (
							<ChartsSerie key={ serieIndex } serie={serie} label={this.state.series[serieIndex]} style={{ height: 'auto'}}>
								{serie => serie.map((item, itemIndex) => {
									const style = {
										width: item/(this.getMax()) * 100 + '%',
										opacity: (item/this.getMax() + .05)
									}
									return <ChartsItem style={this.getStyle(item, itemIndex, style)} key={itemIndex} item={item} color={this.state.colors[itemIndex]}/>
								})}
							</ChartsSerie>
						)
						})
					}
				</Charts>

				<Legend labels={this.state.labels} colors={this.state.colors}/>
			</section>
		);
	}
}

class Charts extends React.PureComponent {
	static defaultProps = {
		additionalClasses: []
	}

	constructor(props) {
		super(props)
	}

	getClasses() {
		let result = ['Charts'],
			additionalClasses = typeof this.props.additionalClasses === 'string' ? this.props.additionalClasses.split(' ') : this.props.additionalClasses;

		return result = [...result, ...additionalClasses].join(' ');
	}

	render() {
		return (
			<div className={this.getClasses()}>
				{this.props.children(this.props.data)}
			</div>
		)
	}
}

class ChartsSerie extends React.PureComponent {
	static defaultProps = {
		additionalClasses: [],
		style: { height:250 }
	}
	
	constructor(props) {
		super(props);
		// console.log('ChartsSerieLabel:', this.props.label);
	}

	getClasses() {
		let result = ['Charts--serie'],
			additionalClasses = typeof this.props.additionalClasses === 'string' ? this.props.additionalClasses.split(' ') : this.props.additionalClasses;

			return result = [...result, ...additionalClasses].join(' ');
	}

	render() {
		return (
			<div className={this.getClasses()} style={this.props.style}>
				<label>{ this.props.label }</label>
				{this.props.children(this.props.serie)}
			</div>
		)
	}

}

class ChartsItem extends React.PureComponent {
	static defaultProps = {
		additionalClasses: []
	}

	constructor(props) {
		super(props)
	}

	getClasses() {
		let result = ['Charts--item'],
			additionalClasses = typeof this.props.additionalClasses === 'string' ? this.props.additionalClasses.split(' ') : this.props.additionalClasses;

		return result = [...result, ...additionalClasses].join(' ');
	}

	render() {
		return (
			<div className={this.getClasses()} style={ this.props.style }>
				<b style={{ color: this.props.color }}>{ this.props.item }</b>
			</div>
		)
	}
}

class Legend extends React.PureComponent {
	render() {
		return (
			<div className="Legend">
				{ this.props.labels.map((label, idx) => {
					return (
						<div>
							<span className="Legend--color" style={{ backgroundColor: this.props.colors[idx % this.props.colors.length]  }} />
							<span className="Legend--label">{ label }</span>
						</div>
					)
				}) }
			</div>
		)
	}
}