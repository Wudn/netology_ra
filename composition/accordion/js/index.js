'use strict';

const items = [
    {
        title: 'Компоненты',
        content: 'Каждый компонент являются законченной частью пользовательского интерфейса и сам управляет своим состоянием, а композиция компонентов (соединение) позволяет создавать более сложные компоненты. Таким образом, создается иерархия компонентов, причем каждый отдельно взятый компонент независим сам по себе. Такой подход позволяет строить сложные интерфейсы, где есть множество состояний, и взаимодействовать между собой.'
    },
    {
        title: 'Выучил раз, используй везде!',
        content: 'После изучения React вы сможете использовать его концепции не только в браузере, но также и при разработке мобильных приложений с использованием React Native.'
    },
    {
        title: 'Использование JSX',
        content: 'JSX является языком, расширяющим синтаксис стандартного Javascript. По факту он позволяет писать HTML-код в JS-скриптах. Такой подход упрощает разработку компонентов и повышает читаемость кода.'
    }
]

class Accordion extends React.PureComponent {
    constructor(props) {
        super(props)
        this.handleSwitchItem = this.handleSwitchItem.bind(this);
        console.log('AccordionProps', this.props.children)
    }

    handleSwitchItem(event) {
        const target = event.target;
        // // Закрываю секцию если она открыта (закомментировал т.к. по моему в аккордеоне одна из секций должна быть всегда открыта)
        // if (target.closest('section').classList.contains('open')) {
        //     target.closest('section').classList.remove('open');
        //     return;
        // }

        // Переключаюсь на выбранную секцию если она закрыта
        if (target.classList.contains('sectionhead')) {
            Array.from(document.querySelectorAll('main.main section')).forEach(el => {
                if (el.classList.contains('open')) {
                    el.classList.remove('open');
                }
            });
            target.closest('section').classList.add('open');
        }
    }
    
    render() {
        const accordionItemsJSX = this.props.items.map((item, idx) => {
            return <AccordionItem key={idx} item={item} onClick={this.handleSwitchItem} isOpen={idx === 0 ? true : false}/>
        })

        return (
            <main className="main">
                <h2 className="title">React</h2>
                {accordionItemsJSX}
            </main>
        )
    }
}

class AccordionItem extends React.PureComponent {
    constructor(props) {
        super(props)
        // Не хочу использовать состояние, т.к. либо все блоки 
        // this.state = {
        //     isOpen: false
        // }
    }

    render() {
        return (
            <section className={this.props.isOpen ? "section open" : "section"} onClick={this.props.onClick}>
                <button>toggle</button>
                <h3 className="sectionhead">{this.props.item.title}</h3>
                <div className="articlewrap">
                    <div className="article">
                        {this.props.item.content}
                    </div>
                </div>
            </section>
        )
    }
}