'use strict';

class ItemsGallery extends React.PureComponent {
  constructor(props) {
    super(props)
  }
  
  render() {
    return (
      <main>
        {this.props.children(this.props.items)}
      </main>
    )
  }
}

class App extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  // Я не уверен, что этот метот должен быть в App, но как его вызвать из ItemsGallery я не понимаю
  getColor = gender => {
    const relations = {
      unisex: 'black',
      male: 'blue',
      female: 'orange'
    }

    return relations[gender];
  }

  render() {
    return (
      <ItemsGallery items={this.props.items}>
        {items => items.map((item, idx) => {
          return <Item key={idx} color={this.getColor(item.type)} item={item} />;
        })}
      </ItemsGallery>
    )
  }
}
