'use strict';

const getFormattedDate = () => {
  const curDate = new Date(),
        year = curDate.getFullYear(),
        month = curDate.getMonth()+1,
        fullMonth = month < 10 ? `0${month}`: month,
        day = curDate.getDate(),
        fullDay = day < 10 ? `0${day}`: day;

  return `${year}-${fullMonth}-${fullDay}`;
}

const DateInput = props => {
  return (
    <div className="form-group">
      <label>{props.label}</label>
      <input type="text" className="form-control" name={props.name} onChange={props.onChange}
             value={props.value} required={props.required} placeholder="YYYY-MM-DD"/>
    </div>
  )
};

DateInput.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string
}

DateInput.defaultProps = {
  value: getFormattedDate()
}