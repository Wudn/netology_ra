'use strict';

const TextInput = props => {
  return (
    <div className="form-group">
      <label>{props.label}</label>
      <input type={props.type} className="form-control" name={props.name} onChange={props.onChange}
             value={props.value} required={props.required}/>
    </div>
  )
};

TextInput.propTypes = {
  'label': PropTypes.string,
  'type': PropTypes.string,
  'name': PropTypes.string,
  'onChange': PropTypes.func,
  'value': PropTypes.string, // Видимо этот атрибут не нужно делать обязательным, т.к. в html указано что он обязательный
  'required': PropTypes.bool
}
