'use strict';

const profileStyle = {
  border: '1px solid #cccccc',
  borderRadius: '5px',
  width: '100%',
  height: '100%',
  margin: '5px'
};

const imageStyle = {
  width: '200px',
  height: '200px'
};

// Принимает дату и сравнивает с текущей
const compareDates = (date) => {
  if (typeof date === 'string') {
    date = new Date(Date.parse(date));
  }
  const curDate = new Date();
  return date >= curDate;
}

const getFormattedDate = (date) => {
  if (typeof date === 'string') {
    date = new Date(Date.parse(date));
  }
  const year = date.getFullYear(),
        month = date.getMonth()+1,
        fullMonth = month < 10 ? `0${month}`: month,
        day = date.getDate(),
        fullDay = day < 10 ? `0${day}`: day;

  return `${year}-${fullMonth}-${fullDay}`;
}

const Profile = props => {
  return (
    <div className="col-md-4 text-center" style={{marginBottom: '10px'}}>
      <div style={profileStyle}>
        <h2>{props.first_name} {props.last_name}</h2>
        <div>
          <img src={props.img} className="img-thumbnail" style={imageStyle}/>
        </div>
        <p>vk: <a href={props.url}>{props.url}</a></p>
        <p>birthday: <a href={props.birthday}>{props.birthday}</a></p>
      </div>
    </div>
  );
};

Profile.propTypes = {
  first_name: PropTypes.string,
  last_name: PropTypes.string,
  img: PropTypes.string,
  url: (props, propName, componentName) => {
    if (!/^https:\/\/vk.com\/(id[0-9]+|[A-Za-z0-9_-]+)$/.test(props[propName])) {
      return new Error(`Invalid prop ${propName} supplied to ${componentName}. Expecting something like 'https://vk.com/(id[0-9]+|[A-Za-z0-9_-]+)'. Validation failed.`);
    }
  },
  birthday: (props, propName, componentName) => {
    if(!/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test(props[propName])) {
      return new Error(`Invalid prop ${propName} supplied to ${componentName}. Expecting something like 'YYYY-DD-MM', ${propName} = ${props[propName]}. Validation failed.`);
    }
    
    if (compareDates(props[propName])) {
      return new Error(`Извините дата в профиле (${props[propName]}) больше или равна текущей даты (${getFormattedDate(new Date())})`);
    }
    
  }
}

Profile.defaultProps = {
  img: './images/profile.jpg',
  birthday: '1988-03-21'
}