class SubscribeForm extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            isValid: null
        }
    }

    handleInputChange(event) {
        const curTarget = event.currentTarget;
        
        this.setState({
            ...this.state,
            isValid: curTarget.validity.valid
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log('Данные формы улетели');
    }

    render() {
        return (
            <div className="subscribe__form">
                <form className={`form form--subscribe ${this.state.isValid ? 'is-valid' : 'is-error'}`} onSubmit={this.handleSubmit}>
                    <h4 className="form-title">Подписаться:</h4>
                    <SubscribeFormGroup onChange={this.handleInputChange} />
                </form>
            </div>
        )
    }
}

class SubscribeFormGroup extends React.PureComponent {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="form-group">
                <label htmlFor="input-email" className="sr-only">Email</label>
                <input type="email" id="input-email" placeholder="Email" className="form-control" onChange={this.props.onChange}/>
                <div className="form-error">Пожалуйста, проверьте корректность адреса электронной почты</div>
                <button type="submit" className="form-next">
                    <i className="material-icons">keyboard_arrow_right</i>
                </button>
            </div>
        )
    }
}