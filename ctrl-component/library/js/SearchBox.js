class SearchBox extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const value = event.target.value;
        this.props.filterBooks(value);
    }

    render() {
        return (
            <input type="text" placeholder="Поиск по названию или автору" onChange={this.handleInputChange} value={this.props.value}/>
        )
    }
}