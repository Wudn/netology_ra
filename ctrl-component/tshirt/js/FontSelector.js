class FontSelector extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleSelectFont = this.handleSelectFont.bind(this);
    }

    handleSelectFont(event) {
        const fontName = event.currentTarget.dataset.fontName;
        const font = this.props.fonts.find(font => font.name === fontName);
        this.props.onSelect(font);
    }

    render() {
        const templateJSX = this.props.fonts.map((font, idx) => {
            return (
                <div className="grid center font-item" key={idx} onClick={this.handleSelectFont} data-font-name={font.name}>
                    <input type="radio" name="font" value={font.name} id={font.name} checked={this.props.selected === font ? true : false } />
                    <label htmlForm={font.name} className="grid-1">
                        <PictureFont text="abc" path={font.path} />
                    </label>
                </div>
            )
        })
        return (
            <div className="font-picker">
                {templateJSX}
            </div>
        )
    }
}