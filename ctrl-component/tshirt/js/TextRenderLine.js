const TextRenderLine = ({value, onChange}) => {
	const handleTextareaChange = (event) => {
		const regEx = /^[a-zA-Z ]*$/;
		const userText = event.target.value.toLowerCase();
	
		if (regEx.test(userText)) {
			onChange(userText);
		}
	};

	return (
		<div className="type-text">
			<textarea name="text" id="font-text" cols="30" rows="2" placeholder="Введите текст для футболки" onChange={handleTextareaChange}></textarea>
		</div>
	);
};
