'use strict';

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            links: [
                {
                    title: 'Главная',
                    url: '/'
                },
                {
                    title: 'Дрифт-такси',
                    url: '/drift'
                },
                {
                    title: 'Time Attack',
                    url: '/timeattack'
                },
                {
                    title: 'Forza Karting',
                    url: '/forza'
                },
            ]
        }
    }

    render() {
        return (
            <nav className="menu">
                {this.state.links.map((link, idx) => {
                    return <NavLink key={idx} exact activeClassName="menu__item-active" className="menu__item" to={link.url}>{link.title}</NavLink>
                })}
            </nav>
        )
    }
}