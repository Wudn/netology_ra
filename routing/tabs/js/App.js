'use strict'

class App extends React.PureComponent {

    render() {
        return(
            <Router>
                <div>
                    <div className="tabs">
                        <nav className="tabs__items">
                            <NavLink exact activeClassName="tabs__item-active" className="tabs__item" to={'/'}>Рефераты</NavLink>
                            <NavLink exact activeClassName="tabs__item-active" className="tabs__item" to={'/creator'}>Криэйтор</NavLink>
                            <NavLink exact activeClassName="tabs__item-active" className="tabs__item" to={'/fortune'}>Гадалка</NavLink>
                        </nav>
                        <div className="tabs__content">
                            <Switch>
                                <Route path="/creator" component={Creator} />
                                <Route path="/fortune" component={Fortune} />
                                <Route path="/" component={Essay} />
                            </Switch>
                        </div>
                    </div>
                </div>
            </Router>
        )
    }
}