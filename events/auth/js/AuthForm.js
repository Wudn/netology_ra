'use strict';

const AuthForm = (props) => {
    
    let nameEl,
        emailEl,
        passwdEl;

    const handle = (event) => {
        event.preventDefault();
        // console.log(nameEl, emailEl, passwdEl);
        if (emailEl.value.length <= 0) {
            return;
        }
        const user = {
            name: nameEl.value,
            email: emailEl.value,
            password: passwdEl.value
        }

        if (props.onAuth && (typeof props.onAuth === 'function')) {
            props.onAuth(user);
        }
    }

    const checkEmail = (event) => {
        const regex = /^[\w@.-]+$/gm;
        const val = event.currentTarget.value;

        if (!regex.test(val)) {
            event.currentTarget.value = val.substring(0, val.length - 1);
            return;
        }
    }

    const checkPasswd = (event) => {
        const regex = /^(\w)+$/g;
        const val = event.currentTarget.value;

        if (!regex.test(val)) {
            event.currentTarget.value = val.slice(0, -1);
            return;
        }
    }

    return (
        <form className="ModalForm" action="/404/auth/" method="POST" onSubmit={handle}>
            <div className="Input">
                <input ref={element => nameEl = element} required type="text" placeholder="Имя" />
                <label></label>
            </div>
            <div className="Input">
                <input ref={element => emailEl = element} required type="email" placeholder="Электронная почта" onChange={checkEmail}/>
                <label></label>
            </div>
            <div className="Input">
                <input ref={element => passwdEl = element} required type="password" placeholder="Пароль" onChange={checkPasswd}/>
                <label></label>
            </div>
            <button type="submit">
                <span>Войти</span>
                <i className="fa fa-fw fa-chevron-right"></i>
            </button>
        </form>
    )
}