'use strict';

const FeedbackForm = props => {
    const data = props.data;
    // console.log(data);
    const handle = (event) => {
        event.preventDefault();
        // console.log('karamba!');
        // props.onSubmit('Данные формы отправлены!');
        props.onSubmit(JSON.stringify(data));
    }

    const Salutation = ({salutation}) => {
        let mr,
            mrs,
            ms;
        // {mr ? defaultChecked : null}
        switch (salutation) {
            case 'Мистер':
                mr = true;
                break;
            case 'Мисис':
                mrs = true;
                break;
            case 'Мис':
                ms = true;
                break;
            default:
                break;
        }
        return (
            <div className="contact-form__input-group">
                <input className="contact-form__input contact-form__input--radio" id="salutation-mr" name="salutation" type="radio" value="Мистер" defaultChecked={mr}/> 
                <label className="contact-form__label contact-form__label--radio" htmlFor="salutation-mr">Мистер</label>
                <input className="contact-form__input contact-form__input--radio" id="salutation-mrs" name="salutation" type="radio" value="Мисис" defaultChecked={mrs}/>
                <label className="contact-form__label contact-form__label--radio" htmlFor="salutation-mrs">Мисис</label>
                <input className="contact-form__input contact-form__input--radio" id="salutation-ms" name="salutation" type="radio" value="Мис"defaultChecked={ms} />
                <label className="contact-form__label contact-form__label--radio" htmlFor="salutation-ms">Мис</label>
            </div>
        )
    }

    const Name = ({fname}) => {
        // console.log('fname', fname);
        return (
            <div className="contact-form__input-group">
                <label className="contact-form__label" htmlFor="name">Имя</label>
                <input className="contact-form__input contact-form__input--text" id="name" name="name" type="text" value={fname}/>
            </div>
        )
    }

    const Email = ({email}) => {
        // console.log('e', email);
        return (
            <div className="contact-form__input-group">
                <label className="contact-form__label" htmlFor="email">Адрес электронной почты</label>
                <input className="contact-form__input contact-form__input--email" id="email" name="email" type="email" value={email}/>
            </div>
        )
    }

    const Subject = ({subject}) => {
        // console.log('s', subject);
        return (
            <div className="contact-form__input-group">
                <label className="contact-form__label" htmlFor="subject">Чем мы можем помочь?</label>
                <select className="contact-form__input contact-form__input--select" id="subject" name="subject" defaultValue={subject}>
                    <option>У меня проблема</option>
                    <option>У меня важный вопрос</option>
                </select>
            </div>
        )
    }

    const Message = ({message}) => {
        // console.log('m', message);
        return (
            <div className="contact-form__input-group">
                <label className="contact-form__label" htmlFor="message">Ваше сообщение</label>
                <textarea className="contact-form__input contact-form__input--textarea" id="message" name="message" rows="6" cols="65" value={message}></textarea>
            </div>
        )
    }

    const Snacks = ({snacks}) => {
        // console.log('sn', snacks);
        // snacks = [];
        // snacks.push('хлеб', 'пицца');
        // let idx = snacks.indexOf('пицца');
        // if (idx > -1) {
        //     snacks.splice(idx, 1);
        // }
        // console.log('idx', idx);
        let pizza,
            pie;

        for (let snack of snacks) {
            if (snack.toLowerCase() === 'пицца'){
                pizza = true;
            }
            if (snack.toLowerCase() === 'пирог') {
                pie = true;
            }
        }

        return (
            <div className="contact-form__input-group">
                <p className="contact-form__label--checkbox-group">Хочу получить:</p>
                <input className="contact-form__input contact-form__input--checkbox" id="snacks-pizza" name="snacks" type="checkbox" value="пицца" checked={pizza}/>
                <label className="contact-form__label contact-form__label--checkbox" htmlFor="snacks-pizza">Пиццу</label>
                <input className="contact-form__input contact-form__input--checkbox" id="snacks-cake" name="snacks" type="checkbox" value="пирог" checked={pie}/>
                <label className="contact-form__label contact-form__label--checkbox" htmlFor="snacks-cake">Пирог</label>
            </div>
        )
    }

    return (
        <form className="content__form contact-form">
            <div className="testing">
                <p>Чем мы можем помочь?</p>
            </div>
            <Salutation salutation={data.salutation}/>
            <Name fname={data.name}/>
            <Email email={data.email}/>
            <Subject subject={data.subject}/>
            <Message message={data.message}/>
            <Snacks snacks={data.snacks}/>
            <button className="contact-form__button" type="submit" onClick={handle}>Отправить сообщение!</button>
            <output id="result" />
        </form>
    )
}

FeedbackForm.defaultProps = {
    data: {
        email: '',
        message: '',
        name: '',
        salutation: '',
        snacks: [],
        subject: ''
    }
}
