'use strict';

const VIEW_LIST = "view_list";
const VIEW_MODULE = "view_module";

class App extends React.Component {
  constructor(props) {
    super(props);
    // console.log('props', props);
    this.switchIconName = this.switchIconName.bind(this);
    this.state = {
      iconName: VIEW_MODULE,
      isCardView: true
    }
  }

  switchIconName() {
    // console.log('switchIconName!');
    this.setState({
      iconName: this.state.iconName === VIEW_MODULE ? VIEW_LIST : VIEW_MODULE,
      isCardView: this.state.iconName === VIEW_MODULE ? false : true
    });
  }

  getIconName() {
    // console.log('getIconName', this.state.iconName);
    return this.state.iconName;
  }

  getIsCardView() {
    return this.state.isCardView;
  }

  render() {
    return (
      <div>
        <div className="toolbar">
          <IconSwitch
            icon={this.getIconName()}
            onSwitch={this.switchIconName} />
        </div>
        {this.renderLayout(this.getIsCardView())}
      </div>
    );
  }

  renderLayout(cardView) {
    if (cardView) {
      return (
        <CardsView
          layout={this.props.layout}
          cards={this.getShopItems(this.props.products, cardView)} />
      );
    }
    return (<ListView items={this.getShopItems(this.props.products, cardView)} />);
  }

  getShopItems(products, cardView) {
    return products.map(product => {
      let cardProps = {
        title: product.name,
        caption: product.color,
        img: product.img,
        price: `$${product.price}`
      };
      if (cardView) {
        return (
          <ShopCard {...cardProps}/>
        );
      }
      return (<ShopItem {...cardProps}/>)
    });
  }
}
