'use strict';

class App extends React.PureComponent {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.toggleOpen = this.toggleOpen.bind(this);

    this.state = {
      active: 'Profile Information',
      open: false
    }
  }
  
  // onClick={this.handleChange.bind(this, option)}
  handleChange(event) {
    this.setState({
      active: event.target.dataset.option
    });
  }

  toggleOpen() {
    this.setState({
      open: !this.state.open
    });
  }

  render() {
    return (
      <div className="container">
        <div className={`dropdown-wrapper ${this.state.open ? "open" : ""}`} >
          <button className={"btn"} onClick={this.toggleOpen} >
            <span>Account Settings</span>
            <i className="material-icons">public</i>
          </button>
          <ul className="dropdown">
            {this.props.options.map((option, i) => (
              <li
                className={option === this.state.active ? "active" : ""}
                onClick={this.handleChange} >
                  <a href="#" data-option={option}>{option}</a>
              </li>
            ))}
          </ul>
        </div>
      </div>
    )
  }
}

App.defaultProps = {
  options: []
}