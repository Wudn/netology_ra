'use strict'

class App extends React.PureComponent {
  constructor(props) {
    super(props);
    this.selectFilter = this.selectFilter.bind(this);
    this.state = {
      selected: 'All'
    }
  }

  selectFilter(filter) {
    this.setState({
      selected: filter
    });
  }

  getFilteredProjects() {
    if (this.state.selected === 'All') {
      return this.props.projects;
    }
    return this.props.projects.filter(project => project.category === this.state.selected);
  }

  render() {
    return (
      <div>
        <Toolbar
          filters={this.props.filters}
          selected={this.state.selected}
          onSelectFilter={this.selectFilter} />
        <Portfolio projects={this.getFilteredProjects()} />
      </div>
    )
  }
}
