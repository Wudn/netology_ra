'use strict';

function Stars({count}) {
  let stars = null;

  if (!isNaN(count) && (count >= 1 && count <= 5)) {
    count = Math.floor(count);
    stars = Array(count).fill().map((_, idx) => <li key={idx}><Star /></li>);
  }
  
  return (
    <ul className="card-body-stars u-clearfix">
      {stars}
    </ul>
    )
}

Stars.defaultProps = {
  count: 0
};
