'use strict';

const MessageHistory = ({list}) => {

    let messages = null;

    if (list.length > 0) {
        messages = list.map((message) => {
            let {id, from, type} = message,
                CustomMessage;
    
            switch (type) {
                case 'response':
                    CustomMessage = Response;
                    break;
                case 'message':
                    CustomMessage = Message;
                    break;
                case 'typing':
                    CustomMessage = Typing;
                    break;
                default:
                    break;
            }
    
            return <CustomMessage key={id} from={from} message={message}/>
        });
    }
    
    return (
        <ul>
            {messages}
        </ul>
    );
}

MessageHistory.defaultProps = {
    list: []
}