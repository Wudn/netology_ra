const Listing = ({items}) => {
    let listItems = null;
    
    if (items.length > 0) {
        const fullPrice = (currency_code, price) => {
            const obj = {
                'USD': '$',
                'EUR': '€'
            }

            if (!(currency_code in obj)) {
                return `${price} ${currency_code}`;
            }

            return `${obj[currency_code]}${price}`;
        };

        const checkQuantityLevel = (quantity) => {
            let quantityLevel = 'low';
            if (quantity <= 10) {
                quantityLevel = 'low';
            }
            if (quantity > 10 && quantity <= 20) {
                quantityLevel = 'medium';
            }
            if (quantity > 20) {
                quantityLevel = 'high';
            }
            
            return quantityLevel;
        }

        listItems = items.filter(item => item.state === 'active').map(({listing_id, url, MainImage, title, currency_code, price, quantity}) => {
            return (
                <div key={listing_id} className="item">
                    <div className="item-image">
                        <a href={url}>
                            <img src={MainImage.url_570xN}/>
                        </a>
                    </div>
                    <div className="item-details">
                        <p className="item-title">{title.length > 50 ?  `${title.substring(0, 50)}…` : title}</p>
                        <p className="item-price">{fullPrice(currency_code, price)}</p>
                        <p className={`item-quantity level-${checkQuantityLevel(quantity)}`}>{`${quantity} left`}</p>
                    </div>
                </div>
            );
        });
    }

    return (
        <div className="item-list">
            {listItems}
        </div>
    );
}

Listing.defaultProps = {
    items: []
}