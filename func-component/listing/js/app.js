'use strict';

fetch('https://neto-api.herokuapp.com/etsy', {  
    method: 'GET' 
})
.then(data => data.json())  
.then(items => {
    ReactDOM.render(<Listing items={items}/>, document.querySelector('#root'));
})
.catch(function (error) {
    console.log('Request failed', error);
});