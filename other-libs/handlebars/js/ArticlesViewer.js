class ArticlesViewer extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    // Отрисовать JSX шаблон
    getArticles() {
        return this.props.articles.map((article, idx) => (
            <article className="article" key={idx}>
                <h1>{ article.subject }</h1>
                <p>{ article.body }</p>
            </article>
        ));
    }

    render() {
        // const articles = this.getArticles();
        // console.log('articles', articles);
        return (
            <div>
                {this.getArticles()}
            </div>
            
        )
    }
}