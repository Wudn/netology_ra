function Calendar(props) {
    const today = props.date;
    const day = today.getDate();
    const monthNumber = today.getMonth();
    const year = today.getFullYear();
    const monthNamesDate = [
        'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня',
        'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'
    ];
    const monthNamesTitle = [
        'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
        'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
    ];

    const rangexy = (start, end) => Array.from({length: (end+1 - start)}, (v, i) => i + start);
    
    function endFirstWeek(firstDate, firstDay) {
        if (! firstDay) {
            return 7 - firstDate.getDay();
        }
        if (firstDate.getDay() < firstDay) {
            return firstDay - firstDate.getDay();
        } else {
            return 7 - firstDate.getDay() + firstDay;
        }
    }
    
    function getWeeksStartAndEndInMonth(month, year, start=1) {
        let weeks = [],
            firstDate = new Date(year, month, 1),
            lastDate = new Date(year, month + 1, 0),
            numDays = lastDate.getDate();
        
        let end = endFirstWeek(firstDate, 1);
        while (start <= numDays) {
            weeks.push({start: start, end: end});
            start = end + 1;
            end = end + 7;
            end = start === 1 && end === 8 ? 1 : end;
            if (end > numDays) {
                end = numDays;
            }
        }
        return weeks;
    }

    const weeks = getWeeksStartAndEndInMonth(monthNumber, year);

    const calendarDaySlots = weeks.length * 7;
    
    // console.log('weeks', weeks);
    // console.log('calendarDaySlots', calendarDaySlots);

    const offsetCalendarDays = calendarDaySlots-(new Date(year, monthNumber + 1, 0).getDate());
    const dayOfWeekByDate = new Date(year, monthNumber, weeks[0].start).getDay();

    const offsetCalendarDaysPrev = dayOfWeekByDate ? [...Array(dayOfWeekByDate-1).keys()].length : 6;
    const offsetCalendarDaysNext = offsetCalendarDays-offsetCalendarDaysPrev;

    // console.log('offsetCalendarDaysPrev', offsetCalendarDaysPrev);
    // console.log('offsetCalendarDaysNext', offsetCalendarDaysNext);

    let weeksJSX = weeks.map((item, index) => {
        let weekDaysJSX,
            firstDayOfWeekNumber = new Date(year, monthNumber, item.start).getDay(),
            lastDayOfWeekNumber = new Date(year, monthNumber, item.end).getDay();
        
        function checkDate(item, offset=[]) {
            let classNames = [];
            if (item === today.getDate() && !offset.includes(item)) {
                classNames.push('ui-datepicker-today');
            }
            if(item <=0) {
                classNames.push('ui-datepicker-other-month');
            }
            if (offset.includes(item)){
                classNames.push('ui-datepicker-other-month');
            }
            return classNames.join(' ');
        }

        if (firstDayOfWeekNumber === 1) {
            weekDaysJSX = rangexy(item.start, item.end).map((item, index) => <td className={checkDate(item)}>{item}</td>);
        }

        if (firstDayOfWeekNumber !== 1) {
            function transformToDayOfMonth(number) {
                return new Date(year, monthNumber, number).getDate();
            }

            // console.log('dayOfWeekByDate', dayOfWeekByDate);
            let weekDays = [];
            for (let i = 0; i < offsetCalendarDaysPrev; i++) {
                weekDays.push(0-i);
            }
            
            
            weekDays = weekDays.concat(rangexy(item.start, item.end)).sort((a,b) => a-b);
            // console.log('weekDays', weekDays);
            weekDaysJSX = weekDays.map((item, index) => <td className={checkDate(item)}>{transformToDayOfMonth(item)}</td>);
        }
        if (lastDayOfWeekNumber !== 0) {
            function transformToDayOfMonth(number) {
                return new Date(year, monthNumber, number).getDate();
            }
            let offsetWeekDays = [];
            for (let i = 1; i <= offsetCalendarDaysNext; i++) {
                offsetWeekDays.push(0+i);
            }
            let fullWeekDays = rangexy(item.start, item.end).concat(offsetWeekDays);
            
            weekDaysJSX = fullWeekDays.map((item, index) => <td className={checkDate(item, offsetWeekDays)}>{transformToDayOfMonth(item)}</td>);
        }
        return (
            <tr key={index}>
                {weekDaysJSX}
            </tr>
        )
    });

    return (
        <div className="ui-datepicker">
            <div className="ui-datepicker-material-header">
                <div className="ui-datepicker-material-day">Среда</div>
                <div className="ui-datepicker-material-date">
                <div className="ui-datepicker-material-day-num">{day}</div>
                <div className="ui-datepicker-material-month">{monthNamesDate[monthNumber]}</div>
                <div className="ui-datepicker-material-year">{year}</div>
                </div>
            </div>
            <div className="ui-datepicker-header">
                <div className="ui-datepicker-title">
                <span className="ui-datepicker-month">{monthNamesTitle[monthNumber]}</span>&nbsp;<span className="ui-datepicker-year">{year}</span>
                </div>
            </div>
            <table className="ui-datepicker-calendar">
            <colgroup>
                <col/>
                <col/>
                <col/>
                <col/>
                <col/>
                <col className="ui-datepicker-week-end"/>
                <col className="ui-datepicker-week-end"/>
            </colgroup>
                <thead>
                    <tr>
                        <th scope="col" title="Понедельник">Пн</th>
                        <th scope="col" title="Вторник">Вт</th>
                        <th scope="col" title="Среда">Ср</th>
                        <th scope="col" title="Четверг">Чт</th>
                        <th scope="col" title="Пятница">Пт</th>
                        <th scope="col" title="Суббота">Сб</th>
                        <th scope="col" title="Воскресенье">Вс</th>
                    </tr>
                </thead>
                <tbody>
                    {weeksJSX}
                </tbody>
            </table>
        </div>
    )
}