function Menu(props) {
    // console.log('props', props);
    const {items, opened} = props;
    const menuItems = items.map((item, idx) => <li key={idx}><a href={item.href}>{item.title}</a></li>)
    if (opened) {
        return (
            <div className="menu menu-open">
                <div className="menu-toggle">
                    <span></span>
                </div>
                <nav>
                    <ul>
                        {menuItems}
                    </ul>
                </nav>
            </div>
        )
    }
    return (
        <div className="menu">
            <div className="menu-toggle">
                <span></span>
            </div>
        </div>
    )
}