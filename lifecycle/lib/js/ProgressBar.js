class ProgressBar extends React.Component {

  constructor(props) {
    super(props);
  }

  renderProgressBar(total, completed) {
    this.ctx = this.canvas.getContext('2d');
    this.drawCircle('#4ca89a', 52, 7, 100/100);
    this.drawCircle('#96d6f4', 45, 7, Number(completed * 100 / total).toFixed(0)/100);
  }

  componentWillReceiveProps({total, completed}) {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.renderProgressBar(total, completed);
  }

  componentDidMount() {
    this.renderProgressBar(this.props.total, this.props.completed);
  }

  drawCircle(color, radius, lineWidth, percent) {
    const centerX = this.canvas.width / 2,
          centerY = this.canvas.height / 2;
    percent = Math.min(Math.max(0, percent || 1), 1);
    this.ctx.beginPath();
    this.ctx.arc(centerX, centerY, radius, 0, Math.PI * 2 * percent, false);
    this.ctx.strokeStyle = color;
    this.ctx.lineCap = 'round'; // butt, round or square
    this.ctx.lineWidth = lineWidth;
    this.ctx.stroke();
  }

  render() {
    return (
      <canvas ref={canvas => this.canvas = canvas} id="progressCanvas" className="progress" />
    );
  }
}
