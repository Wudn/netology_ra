// Я без понятия как получить позицию компонента через react, гуглил, потратил много времени,
// но так ничего полезного не нашел =(
// ссылка на компонент возвращает null
// Поэтому делаю через dom...

class SearchBox extends React.Component {

  constructor(props) {
    super(props);
    this.state = { fixed: false };
  }

  componentDidMount() {
    this.handleScroll = this.handleScroll.bind(this);
    window.addEventListener('scroll', this.handleScroll);
  }

  render() {
    return <SearchBoxView fixed={this.state.fixed} />
  }

  componentWillUnmount() { 
    window.removeEventListener( 'scroll', this.handleScroll )
  }

  handleScroll() {
    this.setPosition();
  }

  isFixed() {
    const searchBoxBounds = document.querySelector('.search-box').getBoundingClientRect();
    const searchBoxTopPos = searchBoxBounds.bottom;
    return pageYOffset > searchBoxTopPos;
  }

  setPosition() {
    this.setState({
      ...this.state,
      fixed: this.isFixed()
    })
  }
}
